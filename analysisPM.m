% notice: program written in MATLAB

% Joanna Rocznik
% Applied Physics, Silesian University of Technology
% Gliwice, 2019

% please, credit the copied chunks of code
% but please, do n o t copy mindlessly
% this code is entirely my work, so do have some decency
% if you're planning on stealing it - karma will find you and do its magic

%______________________________________________________________________________________

%1
path1 = 'C:\Users\student\Desktop\data\data\dane-pomiarowe_2019-10-08.csv';
path2 = 'C:\Users\student\Desktop\data\data\dane-pomiarowe_2019-10-09.csv';
path3 = 'C:\Users\student\Desktop\data\data\dane-pomiarowe_2019-10-10.csv';
path4 = 'C:\Users\student\Desktop\data\data\dane-pomiarowe_2019-10-11.csv';
path5 = 'C:\Users\student\Desktop\data\data\dane-pomiarowe_2019-10-12.csv';
path6 = 'C:\Users\student\Desktop\data\data\dane-pomiarowe_2019-10-13.csv';
path7 = 'C:\Users\student\Desktop\data\data\dane-pomiarowe_2019-10-14.csv';

%______________________________________________________________________________________

%1
% in this part of the code i'm assigning paths of the .csv files to the variables

%______________________________________________________________________________________

%2
fileId_1 = fopen(path1);
C1 = textscan(fileId_1, '%q %q %q %q %q %q', 'Delimiter', ';');


fileId_2 = fopen(path2);
C2 = textscan(fileId_2, '%q %q %q %q %q %q', 'Delimiter', ';');


fileId_3 = fopen(path3);
C3 = textscan(fileId_3, '%q %q %q %q %q %q', 'Delimiter', ';');


fileId_4 = fopen(path4);
C4 = textscan(fileId_4, '%q %q %q %q %q %q', 'Delimiter', ';');


fileId_5 = fopen(path5);
C5 = textscan(fileId_5, '%q %q %q %q %q %q', 'Delimiter', ';');


fileId_6 = fopen(path6);
C6 = textscan(fileId_6, '%q %q %q %q %q %q', 'Delimiter', ';');


fileId_7 = fopen(path7);
C7 = textscan(fileId_7, '%q %q %q %q %q %q', 'Delimiter', ';');

%______________________________________________________________________________________

%2
% here i'm creating a variable 'file ID', unique for each of the imported files, in 
%	order for MATLAB to be able to read their contents
% then, using a textscan function, i'm dividing the contents of the files into 
%	a single cell column -> '%q' ensures that the excess quote signs are 
%	omitted when displaying the cell contents;
% -> 'Delimiter' 'tells' MATLAB that following the nearest comma i'm going to 
%	point out the division indicator (here: semicolon ';') in order to 
%	permanently separate columns;
% the above is done for all files

%______________________________________________________________________________________

%3
for i = 2:25
	A(i-1, 1) = datenum(strcat('2019-10-08', {' '}, cell2mat(C1{1}(i))));
	A(i-1, 2) = str2num(cell2mat(C1{6}(i)));
	A(i-1+24, 1) = datenum(strcat('2019-10-09', {' '}, cell2mat(C2{1}(i))));
	A(i-1+24, 2) = str2num(cell2mat(C2{6}(i)));
	A(i-1+48, 1) = datenum(strcat('2019-10-10', {' '}, cell2mat(C3{1}(i))));
	A(i-1+48, 2) = str2num(cell2mat(C3{6}(i)));
	A(i-1+72, 1) = datenum(strcat('2019-10-11', {' '}, cell2mat(C4{1}(i))));
	A(i-1+72, 2) = str2num(cell2mat(C4{6}(i)));
	A(i-1+96, 1) = datenum(strcat('2019-10-12',{' '},cell2mat(C5{1}(i))));
	A(i-1+96, 2) = str2num(cell2mat(C5{6}(i)));
	A(i-1+120, 1) = datenum(strcat('2019-10-13',{' '},cell2mat(C6{1}(i))));
	A(i-1+120, 2) = str2num(cell2mat(C6{6}(i)));
	A(i-1+144, 1) = datenum(strcat('2019-10-14', {' '}, cell2mat(C7{1}(i))));
	A(i-1+144, 2) = str2num(cell2mat(C7{6}(i)));
end

%______________________________________________________________________________________

%3
% in this loop i select rows - from second to twenty fifth - of the first and the 
%	last cell column of each file in order to put them in a single 2x24 matrix
% i'm converting date into a number under which MATLAB saves the given date string 
%	- and time (the latter occupying the first column) from a cell format 
%	into a matrix
% all of the converted data is put into a newly-created matrix A
% the same is done with the six remaining files, with new data added into 
%	matrix A -> '+24', '+48', etc. is the number of 'hours' i add to each 
%	iteration in order to create a matrix representing data collected 
%	over the whole week
% the ones and twos in the parenthesis indicate the column in which the converted 
%	data is to be put; time occupies the first one and the PM10 values
%	the second one

%______________________________________________________________________________________

%4
B = max(A(:,2));

for c = 1:size(A);
	if A(c, 2) == B;
		r = c;
	end
end

disp('The measurments were conducted throughtout one (1) week from the 8th to the 14th of October 2019. Data from each day was saved in separate .csv files and within each file the measured mean value of PM10 was assigned to a particular hour of the day. A single file consists of 24 differing PM10 values.')
disp('The maximum PM10 value is:')
disp(B)
disp('and it occurred on')
disp(datestr(A(r, 1)))
%______________________________________________________________________________________

%4
% i'm finding the maximum value of PM10 throughout the whole week (matrix A, column 2) 
%	and assigning it to a newly created variable B
% then, i'm creating a loop in which i'm looking for a corresponding date -> i'm 
%	looking for a day on which the levels of PM10 were the highest
% in the very last part of the code (pt 11) i'm saving the maximum value of PM10 and
%	the hour of the day on which it occurred to a .dat file
% using function 'disp' i'm displaying on the screen the requested basic information

%______________________________________________________________________________________

%5
for h = 2:25
	hrs1(h-1, 1) = str2num(strrep(cell2mat(C1{1}(h)), ':', ''));
	hrs1(h-1, 2) = str2num(cell2mat(C1{6}(h)));

	hrs2(h-1, 1) = str2num(strrep(cell2mat(C2{1}(h)), ':', ''));
	hrs2(h-1, 2) = str2num(cell2mat(C2{6}(h)));

	hrs3(h-1, 1) = str2num(strrep(cell2mat(C3{1}(h)), ':', ''));
	hrs3(h-1, 2) = str2num(cell2mat(C3{6}(h)));

	hrs4(h-1, 1) = str2num(strrep(cell2mat(C4{1}(h)), ':', ''));
	hrs4(h-1, 2) = str2num(cell2mat(C4{6}(h)));

	hrs5(h-1, 1) = str2num(strrep(cell2mat(C5{1}(h)), ':', ''));
	hrs5(h-1, 2) = str2num(cell2mat(C5{6}(h)));

	hrs6(h-1, 1) = str2num(strrep(cell2mat(C6{1}(h)), ':', ''));
	hrs6(h-1, 2) = str2num(cell2mat(C6{6}(h)));

	hrs7(h-1, 1) = str2num(strrep(cell2mat(C7{1}(h)), ':', ''));
	hrs7(h-1, 2) = str2num(cell2mat(C7{6}(h)));
end


%______________________________________________________________________________________

%5
% the loop above 'divides' matrix A, which presents data of the whole week, into 
%	original seven days (matrixes 'hrs[number]')
% the first columns representing hours (originally - string format) are converted 
%	into numbers; the delimiter ';' is removed and the hour has a fomat of 
%	[HOUR]00, e.g. for 1 a.m. it is: 100 and for 6 p.m. it is: 1800

%______________________________________________________________________________________

%6
meanDay1 = 0;
meanDay2 = 0;
meanDay3 = 0;
meanDay4 = 0;
meanDay5 = 0;
meanDay6 = 0;
meanDay7 = 0;

for d = 7:17;
	meanDay1 = meanDay1 + (hrs1(d, 2))/11;
	meanDay2 = meanDay2 + (hrs2(d, 2))/11;
	meanDay3 = meanDay3 + (hrs3(d, 2))/11;
	meanDay4 = meanDay4 + (hrs4(d, 2))/11;
	meanDay5 = meanDay5 + (hrs5(d, 2))/11;
	meanDay6 = meanDay6 + (hrs6(d, 2))/11;
	meanDay7 = meanDay7 + (hrs7(d, 2))/11;
end

%______________________________________________________________________________________

%6
% in this part of the code i'm looking for the mean value of PM10 throughout each day
%	(between 7 a.m. and 5 p.m.)
% the 'meanDay[number]' variable is the starting point value to which i'm going to add
%	the actual mean values ( -> the contents of the loop)

%______________________________________________________________________________________

%7
sumNight1_1 = 0;
sumNight1_2 = 0;

sumNight2_1 = 0;
sumNight2_2 = 0;

sumNight3_1 = 0;
sumNight3_2 = 0;

sumNight4_1 = 0;
sumNight4_2 = 0;

sumNight5_1 = 0;
sumNight5_2 = 0;

sumNight6_1 = 0;
sumNight6_2 = 0;

sumNight7_1 = 0;
sumNight7_2 = 0;

for n = 18:24;
	sumNight1_1 = sumNight1_1 + hrs1(n, 2);
end

for n = 1:6;
	sumNight1_2 = sumNight1_2 + hrs2(n, 2);
end


for n = 18:24;
	sumNight2_1 = sumNight2_1 + hrs2(n, 2);
end

for n = 1:6;
	sumNight2_2 = sumNight2_2 + hrs3(n, 2);
end


for n = 18:24;
	sumNight3_1 = sumNight3_1 + hrs3(n, 2);
end

for n = 1:6;
	sumNight3_2 = sumNight3_2 + hrs4(n, 2);
end


for n = 18:24;
	sumNight4_1 = sumNight4_1 + hrs4(n, 2);
end

for n = 1:6;
	sumNight4_2 = sumNight4_2 + hrs5(n, 2);
end


for n = 18:24;
	sumNight5_1 = sumNight5_1 + hrs5(n, 2);
end

for n = 1:6;
	sumNight5_2 = sumNight5_2 + hrs6(n, 2);
end


for n = 18:24;
	sumNight6_1 = sumNight6_1 + hrs6(n, 2);
end

for n = 1:6;
	sumNight6_2 = sumNight6_2 + hrs7(n, 2);
end


for n = 18:24;
	sumNight7_1 = sumNight7_1 + hrs7(n, 2);
end


meanNight1 = (sumNight1_1 + sumNight1_2)/13;
meanNight2 = (sumNight2_1 + sumNight2_2)/13;
meanNight3 = (sumNight3_1 + sumNight3_2)/13;
meanNight4 = (sumNight4_1 + sumNight4_2)/13;
meanNight5 = (sumNight5_1 + sumNight5_2)/13;
meanNight6 = (sumNight6_1 + sumNight6_2)/13;
meanNight7 = (sumNight7_1)/7;

%______________________________________________________________________________________

%7
% this part of the code is exactly the same as part 6 but extended by the mid-step of
%	counting sums of PM10 for each night; a single night consists of an evening
%	part (6 p.m. - midnight) of one day and a very-early-in-the-morning part
%	(1 a.m. - 6 a.m.) of the next day; the addition process is more complex 
%	than previously

%______________________________________________________________________________________

%8
meanNight = (meanNight1 + meanNight2 + meanNight3 + meanNight4 + meanNight5 + meanNight6 + meanNight7)/7;
meanDay = (meanDay1 + meanDay2 + meanDay3 + meanDay4 + meanDay5 + meanDay6 + meanDay7)/7;

%______________________________________________________________________________________

%8
% here i'm calculating the mean values of PM10 levels during the day and during the 
%	night on the whole

%______________________________________________________________________________________

%9
day = [meanDay1; meanDay2; meanDay3; meanDay4; meanDay5; meanDay6; meanDay7];
night = [meanNight1; meanNight2; meanNight3; meanNight4; meanNight5; meanNight6; meanNight7];

stdDay = (std(day))/(sqrt(7));

stdNight = (std(night))/(sqrt(7));

%______________________________________________________________________________________

%9
% first, i'm creating two 1x7 matrixes of the mean values of PM10 during the night 
%	and during the day, respectively, in order to use the std function
% then, i'm calculating the standard deviation of the mean values of PM10 during the 
%	night and day, respectively, using the std function

%______________________________________________________________________________________

%10
t = (abs(meanNight - meanDay))/(sqrt(((stdNight)^2) + ((stdDay)^2)));

result = 0;

if t < 2;
	result = 'The difference is statisically insignificant.';
	
else;
	result = 'The difference is statisically significant.';
end

fprintf(result)

%______________________________________________________________________________________

%10
% here i'm conducting the Student's test (t) in order to find out whether the time of
%	the day is significant to the levels of PM10
% in the loop i'm checking whether the result is statistically significant 
%	(the Student's test)
% then, i'm printing out the variable 'result' with assigned answer (string)

%______________________________________________________________________________________

%11
fid = fopen('PMresults.txt','wt');
fprintf(fid, '%s', '-> The measurments were conducted throughtout one (1) week from the 8th to the 14th of October 2019.');
fprintf(fid, '\n%s', 'Data from each day was saved in separate .csv files and within each file the measured mean value of PM10 was assigned to a particular hour of the day.');
fprintf(fid, '\n%s', 'A single file consists of 24 differing PM10 values.');

fprintf(fid, '\n\n%s', '-> The maximum value of measured PM10 throughout the whole week (on a particular hour of a particular day):');
fprintf(fid, '\n\n%s', datestr(A(r, 1)));
fprintf(fid, '    %s    ', '->');
fprintf(fid,'	%.4f', B);

fprintf(fid, '\n\n%s', '-> The Student_s test concludes that for t < 2 the difference is negligible and for t > 3 the difference is statistically important.');
fprintf(fid, '\n\n%s', '-> As can be seen from the analysis, the value of t is:');
fprintf(fid, '\n%s', 't  =');
fprintf(fid, '  %f', t);
fprintf(fid, '%s', '      (from the loop:   ');
fprintf(fid, '%s', result);
fprintf(fid, '%s', ')');
fprintf(fid, '\n%s', 'which is smaller than 2, thus the difference of mean results is negligible.');
fclose(fid);

%______________________________________________________________________________________

%11
% lastly, i'm saving the most important results into the .dat file:
%	-> summing up the examined period - the 'when's and the 'what's;
%	-> displaying the maximum value of PM10 and when it occurred;
%	-> description, result and conclusion regarding the conducted Student's test 
%		- including the result from the loop